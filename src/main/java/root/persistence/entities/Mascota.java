//
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sthephania
 */
@Entity
@Table(name = "Mascotas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mascota.findAll", query = "SELECT m FROM Mascota m"),
    @NamedQuery(name = "Mascota.findByMascID", query = "SELECT m FROM Mascota m WHERE m.mascID = :mascID"),
    @NamedQuery(name = "Mascota.findByMascNombre", query = "SELECT m FROM Mascota m WHERE m.mascNombre = :mascNombre"),
    @NamedQuery(name = "Mascota.findByMascRaza", query = "SELECT m FROM Mascota m WHERE m.mascRaza = :mascRaza"),
    @NamedQuery(name = "Mascota.findByMascfechaNac", query = "SELECT m FROM Mascota m WHERE m.mascfechaNac = :mascfechaNac"),
    @NamedQuery(name = "Mascota.findByMascChipNum", query = "SELECT m FROM Mascota m WHERE m.mascChipNum = :mascChipNum")})
public class Mascota implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "Masc_ID")
    private String mascID;
    @Size(max = 2147483647)
    @Column(name = "Masc_Nombre")
    private String mascNombre;
    @Size(max = 2147483647)
    @Column(name = "Masc_Raza")
    private String mascRaza;
    @Size(max = 2147483647)
    @Column(name = "Masc_fechaNac")
    private String mascfechaNac;
    @Size(max = 2147483647)
    @Column(name = "Masc_ChipNum")
    private String mascChipNum;

    public Mascota() {
    }

    public Mascota(String mascID) {
        this.mascID = mascID;
    }

    public String getMascID() {
        return mascID;
    }

    public void setMascID(String mascID) {
        this.mascID = mascID;
    }

    public String getMascNombre() {
        return mascNombre;
    }

    public void setMascNombre(String mascNombre) {
        this.mascNombre = mascNombre;
    }

    public String getMascRaza() {
        return mascRaza;
    }

    public void setMascRaza(String mascRaza) {
        this.mascRaza = mascRaza;
    }

    public String getMascfechaNac() {
        return mascfechaNac;
    }

    public void setMascfechaNac(String mascfechaNac) {
        this.mascfechaNac = mascfechaNac;
    }

    public String getMascChipNum() {
        return mascChipNum;
    }

    public void setMascChipNum(String mascChipNum) {
        this.mascChipNum = mascChipNum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mascID != null ? mascID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mascota)) {
            return false;
        }
        Mascota other = (Mascota) object;
        if ((this.mascID == null && other.mascID != null) || (this.mascID != null && !this.mascID.equals(other.mascID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Mascota[ mascID=" + mascID + " ]";
    }
    
}
