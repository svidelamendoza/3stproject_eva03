
package root.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.DAO.MascotaDAO;
import root.DAO.exceptions.NonexistentEntityException;
import root.persistence.entities.Mascota;

@Path("/mascotas")
public class Mascotas_Rest {
    
    //EntityManagerFactory emf = Persistence.createEntityManagerFactory("Mascotas_PU");
    //EntityManager em;
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Mascotas_PU");
    EntityManager em;
    MascotaDAO dao = new MascotaDAO ();

    
//Listar todas las mascotas:
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response ListarMascotas() {
        
        //em = emf.createEntityManager();
        //List<Mascota> listado = em.createNamedQuery("Mascota.findAll").getResultList();
        
        System.out.println("Rescatando datos desde DAO");
        List<Mascota> listado = dao.findMascotaEntities();
        
    return Response.ok(200).entity(listado).build();
    }

    //Busca elementos de acuerdo al ID:
    
    @GET
    @Path("/{mascID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("mascID") String buscarid) {
        
        //em = emf.createEntityManager();
        //Mascota mascota = em.find(Mascota.class, buscarid);
        
        System.out.println("Busca ID desde DAO");
        Mascota Mascota = dao.findMascota(buscarid);
        
    return Response.ok(200).entity(Mascota).build();
    }
    
    //Inserta un nuevo registro:
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevaMasc(Mascota Nuevamascota){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(Nuevamascota);
        em.getTransaction().commit();
        
        return "Mascota creada exitosamente";
    }
    
    //Editar un registro:
    
    @PUT
    public Response editarMasc (Mascota EditarMascota) throws Exception{
        //String resultado = null;
        //em = emf.createEntityManager();
        //em.getTransaction().begin();
        //EditarMascota = em.merge(EditarMascota);
        //em.getTransaction().commit();
        
        Mascota equi = EditarMascota;
        dao.edit(equi);
        
        return Response.ok(EditarMascota).build();
    }
    
    
    //Elimina un registro:
    
    @DELETE
    @Path("/{deleteid}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminarId(@PathParam("deleteid") String deleteid){
        //em = emf.createEntityManager();
        //em.getTransaction().begin();
        //Mascota Elimascota = em.getReference(Mascota.class, deleteid);
        //em.remove(Elimascota);
        //em.getTransaction().commit();
        
        MascotaDAO Elimascota = dao.getEntityManager().getReference(MascotaDAO.class, deleteid);

        
        try {
            dao.destroy(deleteid);
           
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Mascotas_Rest.class.getName()).log(Level.SEVERE, null, ex);
        return Response.ok("Mascota Eliminada! \n Registro:"+Elimascota.toString()).build();
    }
        return null;
           
       }
    
}
