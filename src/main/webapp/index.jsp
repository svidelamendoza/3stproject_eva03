<%-- 
    Document   : index
    Created on : 05-05-2020, 3:39:16
    Author     : Sthephania
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<title>eva03 - CIISA</title>
<script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=_L1SAjry0EaPqcqXhCJ4SsYEI4oPrR5yu-8uDj4W16Zzn-32oi9zZAC-h9TzeLwe4QEm0RUvJwLYTHCX50fE1aTa1RXze2ovtrjsShs-tULUOVz7EBWBzW0xzZfb9yob" charset="UTF-8"></script></head>

<body>
 <div class="demo">
        <div class="container">
            <div class="row text-center">
                <h1 class="heading-title">EVALUACIÓN 03 - REPOSITORIO DE MASCOTAS</h1>
                <br></br>
                <li><b>Nombre Alumno:</b>Sthephania Videla Mendoza</li>
                <li><b>Nombre Profesor:</b>César Cruces</li>
                <br></br>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="pricingTable blue">
                        <div class="pricingTable-header">
                            <br></br>
                            <span><b>OPERACION CRUD - MÉTODO GET</b></span>
                            <br></br>
                        </div>
                        <!-- / <div class="pricingContent">
                            <ul>
                                <li><b>50GB</b> Disk Space</li>
                                <li><b>50</b> Email Accounts</li>
                                <li><b>50GB</b> Monthly Bandwidth</li>
                                <li><b>10</b> subdomains</li>
                                <li><b>15</b> Domains</li>
                            </ul>
                        </div><!-- /  CONTENT BOX-->
                        <div class="pricingTable-sign-up"><!-- BUTTON BOX-->
                            <span class="price-value">Función: Listar / Consultar Mascotas</span>
                            <br></br>
                            <a href="http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/">http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/</a>
                        </div><!-- BUTTON BOX-->
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="pricingTable pink">
                        <div class="pricingTable-header">
                            <br></br>
                            <span><b>OPERACION CRUD - MÉTODO POST</b></span>
                            <br></br>
                        </div>
                        <!--<div class="pricingContent">
                            <ul>
                                <li><b>70GB</b> Disk Space</li>
                                <li><b>70</b> Email Accounts</li>
                                <li><b>70GB</b> Monthly Bandwidth</li>
                                <li><b>15</b> subdomains</li>
                                <li><b>20</b> Domains</li>
                            </ul>
                        </div><!-- /  CONTENT BOX-->
                        <div class="pricingTable-sign-up"><!-- BUTTON BOX-->
                            <span class="price-value">Función: Insertar Mascotas</span>
                            <br></br>
                            <a href="http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/">http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/</a>
                        </div><!-- BUTTON BOX-->
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="pricingTable orange">
                        <div class="pricingTable-header">
                            <br></br>
                            <span><b>OPERACION CRUD - MÉTODO PUT</b></span>
                            <br></br>
                        </div>
                        <!--<div class="pricingContent">
                            <ul>
                                <li><b>80GB</b> Disk Space</li>
                                <li><b>80</b> Email Accounts</li>
                                <li><b>80GB</b> Monthly Bandwidth</li>
                                <li><b>25</b> subdomains</li>
                                <li><b>30</b> Domains</li>
                            </ul>
                        </div><!-- /  CONTENT BOX-->
                        <div class="pricingTable-sign-up"><!-- BUTTON BOX-->
                            <span class="price-value">Función: Editar Mascotas</span>
                            <br></br>
                            <a href="http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/">http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/</a>
                        </div><!-- BUTTON BOX-->
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="pricingTable green">
                        <div class="pricingTable-header">
                            <br></br>
                            <span><b>OPERACION CRUD - MÉTODO DELETE</b></span>
                            <br></br>
                        </div>
                        <!--<div class="pricingContent">
                            <ul>
                                <li><b>90GB</b> Disk Space</li>
                                <li><b>90</b> Email Accounts</li>
                                <li><b>90GB</b> Monthly Bandwidth</li>
                                <li><b>30</b> subdomains</li>
                                <li><b>35</b> Domains</li>
                            </ul>
                        </div><!-- /  CONTENT BOX-->
                        <div class="pricingTable-sign-up"><!-- BUTTON BOX-->
                            <span class="price-value">Función: Eliminar Mascotas</span>
                            <br></br>
                            <a href="http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/{deleteid}">http://localhost:8081/eva03Mascotas-rest-1.0-SNAPSHOT/api/mascotas/{deleteid}</a>
                        </div><!-- BUTTON BOX-->
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
